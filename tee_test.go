/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package utils

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBinaryToUint32(t *testing.T) {
	tmp := []byte("1000")
	a, err := BinaryToUint32(tmp)
	assert.Nil(t, err)
	fmt.Println(a)
}

func TestUint32ToBinary(t *testing.T) {
	tmp := uint32(825241648)
	a := Uint32ToBinary(tmp)
	fmt.Println(a)
	assert.Nil(t, nil)
}
